package zinoviev.oleg.googlesearch.services

sealed trait SearchServiceError { def message: String }

case class ExternalServiceError(message: String) extends SearchServiceError
