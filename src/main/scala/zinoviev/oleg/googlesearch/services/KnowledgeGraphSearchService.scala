package zinoviev.oleg.googlesearch.services

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import com.google.api.client.http.GenericUrl
import com.google.api.client.http.javanet.NetHttpTransport
import play.api.libs.functional.syntax._
import play.api.libs.json.{__, Format, Json}
import zinoviev.oleg.googlesearch.domain.{ApplicationConfig, GoogleSearchRequest, GoogleSearchResponse}
import zinoviev.oleg.googlesearch.services.KnowledgeGraphSearchService.SimpleKnowledgeGraph
import zinoviev.oleg.googlesearch.utils.LoggerLike
import zinoviev.oleg.googlesearch.utils.json._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

class KnowledgeGraphSearchService(googleCredentials: ApplicationConfig)(implicit val system: ActorSystem)
    extends SearchService
    with LoggerLike {

  private val GenericUrl = "https://kgsearch.googleapis.com/v1/entities:search"

  override def search(
    request: GoogleSearchRequest
  )(implicit ec: ExecutionContext): Future[Either[SearchServiceError, GoogleSearchResponse]] = {
    logger.debug("Received request: {}", request)
    Future {
      executeRequest(request) match {
        case Failure(e) =>
          logger.error(e, "Error occurred during search: {}", e.getMessage)
          Left(ExternalServiceError(e.getMessage): SearchServiceError)
        case Success(graph) =>
          val uriOpt = graph.itemListElement match {
            case _ :: y :: _ => Some(y.result.detailedDescription.url)
            case _           => None
          }
          Right(GoogleSearchResponse(uriOpt))
      }
    }
  }

  private def executeRequest(request: GoogleSearchRequest): Try[SimpleKnowledgeGraph] =
    Try {
      val httpTransport = new NetHttpTransport
      val requestFactory = httpTransport.createRequestFactory
      val url = new GenericUrl(GenericUrl)
      url.put("query", request.query)
      url.put("limit", "10")
      url.put("indent", "true")
      url.put("key", googleCredentials.googleApiKey)
      val nativeRequest = requestFactory.buildGetRequest(url)
      val nativeResponse = nativeRequest.execute
      val body = nativeResponse.parseAsString
      Json.parse(body).as[SimpleKnowledgeGraph]
    }
}

object KnowledgeGraphSearchService {
  case class DetailedDescription(url: Uri)
  object DetailedDescription {
    implicit val detailedDescriptionFmt: Format[DetailedDescription] =
      (__ \ "url").format[Uri].inmap(DetailedDescription.apply, unlift(DetailedDescription.unapply))
  }

  case class SimpleKnowledgeResult(name: String, detailedDescription: DetailedDescription)
  object SimpleKnowledgeResult {
    implicit val simpleKnowledgeResultFmt: Format[SimpleKnowledgeResult] = (
      (__ \ "name").format[String] and
        (__ \ "detailedDescription").format[DetailedDescription]
    )(SimpleKnowledgeResult.apply, unlift(SimpleKnowledgeResult.unapply))
  }

  case class SimpleKnowledgeGraphElement(resultScore: Double, result: SimpleKnowledgeResult)
  object SimpleKnowledgeGraphElement {
    implicit val simpleKnowledgeGraphElementFmt: Format[SimpleKnowledgeGraphElement] = (
      (__ \ "resultScore").format[Double] and
        (__ \ "result").format[SimpleKnowledgeResult]
    )(SimpleKnowledgeGraphElement.apply, unlift(SimpleKnowledgeGraphElement.unapply))
  }

  case class SimpleKnowledgeGraph(itemListElement: List[SimpleKnowledgeGraphElement])
  object SimpleKnowledgeGraph {
    implicit val detailedDescriptionFmt: Format[SimpleKnowledgeGraph] =
      (__ \ "itemListElement")
        .formatWithDefault[List[SimpleKnowledgeGraphElement]](List.empty[SimpleKnowledgeGraphElement])
        .inmap(SimpleKnowledgeGraph.apply, unlift(SimpleKnowledgeGraph.unapply))
  }

  def apply(googleCredentials: ApplicationConfig)(implicit system: ActorSystem): SearchService =
    new KnowledgeGraphSearchService(googleCredentials)
}
