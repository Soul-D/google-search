package zinoviev.oleg.googlesearch.services

import akka.actor.ActorSystem
import zinoviev.oleg.googlesearch.domain.SearchType.CustomSearch
import zinoviev.oleg.googlesearch.domain.{GoogleSearchRequest, GoogleSearchResponse, SearchType}

import scala.concurrent.{ExecutionContext, Future}

class SearchCoordinator(serviceRegistry: Map[SearchType, SearchService])(implicit val system: ActorSystem) {
  def search(request: GoogleSearchRequest)(implicit ec: ExecutionContext): Future[Either[SearchServiceError, GoogleSearchResponse]] = {
    val searchType = request.searchType.getOrElse(CustomSearch)
    serviceRegistry(searchType).search(request)
  }
}

object SearchCoordinator {
  def apply(serviceRegistry: Map[SearchType, SearchService])(implicit system: ActorSystem) = new SearchCoordinator(serviceRegistry)
}
