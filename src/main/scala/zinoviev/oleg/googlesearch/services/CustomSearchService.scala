package zinoviev.oleg.googlesearch.services

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.http.{HttpRequest, HttpRequestInitializer}
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.customsearch.Customsearch
import com.google.api.services.customsearch.model.Result
import zinoviev.oleg.googlesearch.domain.{ApplicationConfig, GoogleSearchRequest, GoogleSearchResponse}
import zinoviev.oleg.googlesearch.utils.LoggerLike

import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

class CustomSearchService(googleCredentials: ApplicationConfig)(implicit val system: ActorSystem)
    extends SearchService
    with LoggerLike {
  override def search(
    request: GoogleSearchRequest
  )(implicit ec: ExecutionContext): Future[Either[SearchServiceError, GoogleSearchResponse]] = {
    logger.debug("Received request: {}", request)
    Future {
      executeRequest(request) match {
        case Failure(e) =>
          logger.error(e, "Error occurred during search: {}", e.getMessage)
          Left(ExternalServiceError(e.getMessage): SearchServiceError)
        case Success(result) =>
          val uriOpt = result.collect { case list if list.size >= 2 => Uri(list.tail.head.getFormattedUrl) }
          Right(GoogleSearchResponse(uriOpt))
      }
    }
  }

  private def executeRequest(request: GoogleSearchRequest): Try[Option[List[Result]]] =
    Try {
      val searcher = customSearch()
      val nativeRequest = searcher.cse.list(request.query)
      nativeRequest.setKey(googleCredentials.googleApiKey)
      nativeRequest.setCx(googleCredentials.searchEngineId)
      val nativeResponse = nativeRequest.execute
      Option(nativeResponse.getItems).map(_.asScala.toList)
    }

  private def customSearch() =
    new Customsearch(new NetHttpTransport, new JacksonFactory, new HttpRequestInitializer() {
      override def initialize(request: HttpRequest): Unit = {
        request.setConnectTimeout(googleCredentials.timeoutInMs)
        request.setReadTimeout(googleCredentials.timeoutInMs)
      }
    })
}

object CustomSearchService {
  def apply(googleCredentials: ApplicationConfig)(implicit system: ActorSystem): SearchService =
    new CustomSearchService(googleCredentials)
}
