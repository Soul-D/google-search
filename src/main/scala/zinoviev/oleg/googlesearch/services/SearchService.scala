package zinoviev.oleg.googlesearch.services

import zinoviev.oleg.googlesearch.domain.{GoogleSearchRequest, GoogleSearchResponse}

import scala.concurrent.{ExecutionContext, Future}

trait SearchService {
  def search(request: GoogleSearchRequest)(implicit ec: ExecutionContext): Future[Either[SearchServiceError, GoogleSearchResponse]]
}
