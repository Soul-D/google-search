package zinoviev.oleg.googlesearch

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import zinoviev.oleg.googlesearch.domain.{ApplicationConfig, SearchType}
import zinoviev.oleg.googlesearch.domain.SearchType.{CustomSearch, KnowledgeGraphSearch}
import zinoviev.oleg.googlesearch.services.{CustomSearchService, KnowledgeGraphSearchService, SearchCoordinator, SearchService}
import zinoviev.oleg.googlesearch.utils.LoggerLike
import zinoviev.oleg.googlesearch.web.GoogleSearchRestApi

class Server()(implicit val system: ActorSystem, materializer: ActorMaterializer) extends LoggerLike {
  val config = ConfigFactory.load()
  val host = config.getString("http.host")
  val port = config.getInt("http.port")

  def run(): Unit = {
    logger.info("Starting server...")

    val applicationConfig = ApplicationConfig.load()

    val serviceRegistry: Map[SearchType, SearchService] =
      Map(
        CustomSearch -> CustomSearchService(applicationConfig),
        KnowledgeGraphSearch -> KnowledgeGraphSearchService(applicationConfig)
      )

    val coordinator = SearchCoordinator(serviceRegistry)
    val api = GoogleSearchRestApi(coordinator)

    Http().bindAndHandle(api.routes, host, port)

    logger.info("Server started at the address {}:{}", host, port)
  }
}

object Server {
  implicit val system: ActorSystem = ActorSystem("Server")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  def main(args: Array[String]): Unit =
    new Server().run()
}
