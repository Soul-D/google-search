package zinoviev.oleg.googlesearch.domain

import play.api.libs.functional.syntax._
import play.api.libs.json.{__, Format}

case class GoogleSearchRequest(query: String, searchType: Option[SearchType] = None)

object GoogleSearchRequest {
  implicit val googleSearchRequestFmt: Format[GoogleSearchRequest] =
    (
      (__ \ "query").format[String] and
        (__ \ "searchType").formatNullable[SearchType]
    )(GoogleSearchRequest.apply, unlift(GoogleSearchRequest.unapply))
}
