package zinoviev.oleg.googlesearch.domain

import com.typesafe.config.ConfigFactory

case class ApplicationConfig(searchEngineId: String, googleApiKey: String, timeoutInMs: Int)

object ApplicationConfig {
  def load(): ApplicationConfig = {
    val config = ConfigFactory.load()
    val searchEngineId = config.getString("credentials.search-engine-id")
    val googleApiKey = config.getString("credentials.google-api-key")
    val timeoutInMs = config.getInt("credentials.http-request-timeout")
    ApplicationConfig(searchEngineId = searchEngineId, googleApiKey = googleApiKey, timeoutInMs = timeoutInMs)
  }
}
