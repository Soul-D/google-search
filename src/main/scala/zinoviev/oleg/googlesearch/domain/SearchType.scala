package zinoviev.oleg.googlesearch.domain

import enumeratum.{Enum, EnumEntry}
import play.api.libs.json._

import scala.collection.immutable

sealed trait SearchType extends EnumEntry.Lowercase

object SearchType extends Enum[SearchType] {

  case object CustomSearch extends SearchType
  case object KnowledgeGraphSearch extends SearchType

  implicit val searchTypeFmt: Format[SearchType] = new Format[SearchType] {
    override def reads(json: JsValue): JsResult[SearchType] = json match {
      case JsString(name) =>
        SearchType
          .withNameInsensitiveOption(name)
          .map(JsSuccess(_))
          .getOrElse(JsError(s"Unknown SearchType.($name)"))
      case _ => JsError(s"Wrong SearchType format.")
    }

    override def writes(o: SearchType): JsValue = JsString(o.entryName)
  }

  override val values: immutable.IndexedSeq[SearchType] = findValues
}
