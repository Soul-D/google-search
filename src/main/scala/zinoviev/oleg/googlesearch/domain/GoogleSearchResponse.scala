package zinoviev.oleg.googlesearch.domain

import akka.http.scaladsl.model.Uri
import play.api.libs.functional.syntax._
import play.api.libs.json.{__, Format}
import zinoviev.oleg.googlesearch.utils.json._

case class GoogleSearchResponse(result: Option[Uri])

object GoogleSearchResponse {
  implicit val googleSearchResponseFmt: Format[GoogleSearchResponse] =
    (__ \ "result").formatNullable[Uri].inmap(GoogleSearchResponse.apply, unlift(GoogleSearchResponse.unapply))
}
