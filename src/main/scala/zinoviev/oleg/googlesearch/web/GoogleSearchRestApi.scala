package zinoviev.oleg.googlesearch.web

import akka.actor.ActorSystem
import akka.http.scaladsl.server.{Directives, Route}
import de.heikoseeberger.akkahttpplayjson.PlayJsonSupport
import zinoviev.oleg.googlesearch.domain.GoogleSearchRequest
import zinoviev.oleg.googlesearch.services.SearchCoordinator
import zinoviev.oleg.googlesearch.utils.LoggerLike

class GoogleSearchRestApi(searchCoordinator: SearchCoordinator)(implicit val system: ActorSystem)
    extends Directives
    with LoggerLike
    with DirectivesHelper
    with PlayJsonSupport {
  val routes: Route =
    logRequestResponse() {
      handleRejections(rejectionHandler) {
        handleExceptions(exHandler) {
          extractExecutionContext { implicit ec =>
            pathEndOrSingleSlash {
              (post & entity(as[GoogleSearchRequest])) { request =>
                onSuccess(searchCoordinator.search(request))(completeEither)
              }
            }
          }
        }
      }
    }
}

object GoogleSearchRestApi {
  def apply(searchCoordinator: SearchCoordinator)(implicit system: ActorSystem) =
    new GoogleSearchRestApi(searchCoordinator)
}
