package zinoviev.oleg.googlesearch.web

import akka.http.scaladsl.marshalling.ToResponseMarshaller
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server._
import zinoviev.oleg.googlesearch.services.{ExternalServiceError, SearchServiceError}
import zinoviev.oleg.googlesearch.utils.LoggerLike

trait DirectivesHelper { this: Directives with LoggerLike =>
  protected def completeEither[R: ToResponseMarshaller](either: Either[SearchServiceError, R]): StandardRoute =
    either match {
      case Right(value) => complete(value)
      case Left(err) =>
        err match {
          case e: ExternalServiceError =>
            logger.error("Search failed due to {}", e.message)
            complete(StatusCodes.BadRequest)
        }
    }

  protected def logRequestResponse(): Directive0 = extractRequest.flatMap { request =>
    logger.debug("HttpRequest:\n{}", request)
    mapResponse { response =>
      logger.debug("HttpResponse:\n{}", response)
      response
    }
  }

  protected def exHandler: ExceptionHandler = ExceptionHandler {
    case e: Exception =>
      logger.error(e, "Encountered with: {}", e.getMessage)
      complete(StatusCodes.InternalServerError -> "Cannot process request")
  }

  protected def rejectionHandler: RejectionHandler =
    RejectionHandler
      .newBuilder()
      .handle {
        case x =>
          logger.error("ValidationRejection: {}", x)
          complete(StatusCodes.BadRequest)
      }
      .result()
}
