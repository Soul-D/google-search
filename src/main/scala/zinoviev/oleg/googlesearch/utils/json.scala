package zinoviev.oleg.googlesearch.utils

import akka.http.scaladsl.model.Uri
import play.api.libs.json._

import scala.util.Try

object json {
  implicit val uriFormat = Format[Uri](
    Reads[Uri] {
      case JsString(value) =>
        Try(Uri.parseAbsolute(value))
          .fold(ex => JsError(s"Can not parse $value because of $ex, ${ex.getStackTrace}"), uri => JsSuccess(uri))
      case js => JsError(s"$js should be string encoded")
    },
    Writes[Uri](uri => JsString(uri.toString()))
  )
}
