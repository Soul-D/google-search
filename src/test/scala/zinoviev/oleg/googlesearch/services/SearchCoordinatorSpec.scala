package zinoviev.oleg.googlesearch.services

import akka.actor.ActorSystem
import akka.http.scaladsl.model.Uri
import akka.http.scaladsl.util.FastFuture
import org.scalamock.scalatest.MockFactory
import org.scalatest.{AsyncFlatSpecLike, BeforeAndAfterAll, EitherValues, Matchers}
import zinoviev.oleg.googlesearch.domain.SearchType.{CustomSearch, KnowledgeGraphSearch}
import zinoviev.oleg.googlesearch.domain.{GoogleSearchRequest, GoogleSearchResponse, SearchType}

import scala.concurrent.ExecutionContext

class SearchCoordinatorSpec extends MockFactory with AsyncFlatSpecLike with Matchers with EitherValues with BeforeAndAfterAll {
  implicit val as = ActorSystem(getClass.getSimpleName)
  implicit val ec = as.dispatcher

  val customSearchService = mock[SearchService]
  val knowledgeGraphSearchService = mock[SearchService]

  val serviceRegistry: Map[SearchType, SearchService] =
    Map(CustomSearch -> customSearchService, KnowledgeGraphSearch -> knowledgeGraphSearchService)
  val coordinator = SearchCoordinator(serviceRegistry)

  override def afterAll(): Unit = as.terminate()

  "SearchCoordinator" should "use custom search algorithm if search type is not define in the request" in {
    val searchRequest = GoogleSearchRequest("Book")
    val searchResponse = GoogleSearchResponse(Some(Uri("http://some-book.com")))

    (customSearchService
      .search(_: GoogleSearchRequest)(_: ExecutionContext))
      .expects(searchRequest, *)
      .returning(FastFuture.successful(Right(searchResponse)))
      .once()

    coordinator.search(searchRequest).map { result =>
      result shouldBe 'right
      result.right.value shouldBe searchResponse
    }
  }

  it should "use appropriate search algorithm if search type is define in the request" in {
    val customSearchRequest = GoogleSearchRequest("Book", Some(CustomSearch))
    val customSearchResponse = GoogleSearchResponse(Some(Uri("http://some-custom-book.com")))

    (customSearchService
      .search(_: GoogleSearchRequest)(_: ExecutionContext))
      .expects(customSearchRequest, *)
      .returning(FastFuture.successful(Right(customSearchResponse)))
      .once()

    coordinator.search(customSearchRequest).map { result =>
      result shouldBe 'right
      result.right.value shouldBe customSearchResponse
    }

    val knowledgeGraphSearchRequest = GoogleSearchRequest("Book", Some(KnowledgeGraphSearch))
    val knowledgeGraphSearchResponse = GoogleSearchResponse(Some(Uri("http://some-knowledge-book.com")))

    (knowledgeGraphSearchService
      .search(_: GoogleSearchRequest)(_: ExecutionContext))
      .expects(knowledgeGraphSearchRequest, *)
      .returning(FastFuture.successful(Right(knowledgeGraphSearchResponse)))
      .once()

    coordinator.search(knowledgeGraphSearchRequest).map { result =>
      result shouldBe 'right
      result.right.value shouldBe knowledgeGraphSearchResponse
    }
  }
}
