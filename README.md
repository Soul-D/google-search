# Google search

Simple google search service that uses two different algorithms under the hood.

## Requirements

1. The following environment variables should be set(mandatory):
   * `GOOGLE_API_KEY` - google api key
   * `SEARCH_ENGINE_ID` - search engine id
2. In your account knowledge graph search and custom search should be activated  
3. The environment variable which form address:
   * `APPLICATION_HOST`(if not set, `localhost` is used)
   * `APPLICATION_PORT`(if not set, `8080` is used)

## How to run

```shell
$sbt run
```

## Google search API

Google search application supports two types of the algorithm. If Algorithm is not set in the request, custom search will be used.

Examples of the requests:
1. with `KnowledgeGraphSearch` type
```json
curl --location --request POST 'localhost:8080' \
--header 'Content-Type: application/json' \
--data-raw '{
    "query": "Book",
    "searchType": "KnowledgeGraphSearch"
}'
```
2. with `CustomSearch` type
```json
curl --location --request POST 'localhost:8080' \
--header 'Content-Type: application/json' \
--data-raw '{
   "query": "Book",
   "searchType": "CustomSearch"
}'
```
3. without search type(as default `CustomSearch` is used)
```json
curl --location --request POST 'localhost:8080' \
--header 'Content-Type: application/json' \
--data-raw '{
   "query": "Book"
}'
```

Example of the response, the field `result` is optional and represents uri of the second result from Google(if exists):
```json
{
    "result": "https://books.google.com/"
}
```